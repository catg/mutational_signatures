# Mutational spectrum analysis

- For every substitution is incorporated the context with +/- 2 nucleotides from the reference.  
- Then substitutions are converted to C/T and the contexts to reverse complement to simplify prior analysis.  
- Finally, a matrix with the 96 combinations of substitutions and contexts is generated.
